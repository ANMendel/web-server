#include <cstring>
#include <cstdint>

extern "C" {
#include <unistd.h>
#include <signal.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <sys/time.h>
#include <errno.h>
}

#include <string>
#include <map>
#include <iostream>

using namespace std;

static const uint16_t PortNumber = 3000;
static const size_t MaxEvents = 10000;
static const string Message = "It works! \nClient data:";
static volatile sig_atomic_t StopRequest = 0;

static int create_and_bind()
{
    int sd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); //�������� IPv4 TCP ������. ������� ���������� �������� ���������� ������.)
    if (sd == -1)
    {
        perror("Socket error");
        exit(1);
    }

    sockaddr_in addr; //���������  address family: AF_INET + port in network byte order + internet address
    memset(&addr, 0, sizeof(addr));
    addr.sin_addr.s_addr = INADDR_ANY; // ����� �� ����� ���������� �����������
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PortNumber); //������������ ������� ������ ��������� ����� �� ������� ����, ��������� �� ����������, � ������� ������� ����.


    int bs = bind(sd, (const struct sockaddr*) &addr, sizeof(addr)); //�������� ������ � ����������� ������ (���� � IP)
    if (bs == -1)
    {
        perror("Bind error");
        close(sd);
        exit(1);
    }

    return sd;
}

static void make_socket_non_blocking (int sock_fd)
{
    int flags = fcntl (sock_fd, F_GETFL);
    if (flags == -1)
    {
        perror ("Fcntl error");
        close(sock_fd);
        exit(1);
    }

    flags = fcntl (sock_fd, F_SETFL, flags | O_NONBLOCK);
    if (flags == -1)
    {
        perror ("Fcntl error");
        close(sock_fd);
        exit(1);
    }
}

static void stop_handler(int s)
{
    StopRequest = 1;
}

int main()
{
    int sock_fd = create_and_bind();

    make_socket_non_blocking(sock_fd);

    int status = listen(sock_fd, SOMAXCONN);  //������������� ������, ������ ������� �������� ����������� SOMAXCONN
    if (status == -1)
    {
        perror("Listen error");
        close(sock_fd);
        exit(1);
    }

    int ed = epoll_create1(0); //���������� ���������� �����, ����������� �� ����� ��������� epoll.
    if (ed == -1)
    {
        perror("Epoll_create1 error");
        close(sock_fd);
        exit(1);
    }

    epoll_event event;
    memset(&event, 0, sizeof(event));
    event.data.fd = sock_fd;
    event.events = EPOLLIN | EPOLLET; // �������� ����� ������ �� ������ + ������ Edge Triggered

    status = epoll_ctl(ed, EPOLL_CTL_ADD, sock_fd, &event); //

    if (status == -1)
    {
        perror("Epoll control error");
        close(sock_fd);
        exit(1);
    }

    signal(SIGINT, stop_handler);
    signal(SIGTERM, stop_handler);

    epoll_event *events = new epoll_event[MaxEvents];


    while (!StopRequest)
    {
        int n = epoll_wait(ed, events, MaxEvents, -1);
        if (n == -1)
        {
            perror("Epoll_wait error");
            close(sock_fd);
            exit(1);
        }

        for (int i = 0; i < n; ++i)
        {
            const int fd = events[i].data.fd;
            if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP))
            {
                if (events[i].events & EPOLLERR)
                     perror("Epoll control error");
                close(fd);
                continue;
            }
            else if (fd == sock_fd)
            {
                while (1)
                {
                    struct sockaddr_in in_addr;
                    socklen_t in_addr_size = sizeof(in_addr);
                    int incoming_fd = accept(sock_fd, (sockaddr*)&in_addr, &in_addr_size); //�������� �������� �����������. ����� ��������� ���������� ������� ������ �������� ���������� ����������� ������.
                    if (incoming_fd == -1) {
                        if (EAGAIN == errno || EWOULDBLOCK == errno)
                            break; //���������� ��� �������� ����������
                        else
                        {
                            perror("Accept failed");
                            close(sock_fd);
                            exit(1);
                        }
                    }

                    make_socket_non_blocking(incoming_fd);

                    event.data.fd = incoming_fd;
                    event.events = EPOLLIN | EPOLLET;

                    status = epoll_ctl(ed, EPOLL_CTL_ADD, incoming_fd, &event);
                    if (status == -1)
                    {
                        perror("Epoll control for incoming connection error");
                        close(sock_fd);
                        exit(1);
                    }
                } /* end while(true) */
            }
            else
            {
                printf("Open new connection on descriptor %d\n", events[i].data.fd);
                write(fd, Message.c_str(), Message.length());
                int flag = 0;
                while (1)
                {
                    char buf[512];
                    ssize_t cnt = read(events[i].data.fd, buf, sizeof buf);
                    if (cnt == -1) //���-�� ����� �� ���
                    {
                        if (errno != EAGAIN)
                        {
                            perror("Read error");
                            flag = 1;
                        }
                        break;
                    }
                    else if (cnt == 0) //����� �����
                    {
                        flag = 1;
                        break;
                    }

                    int s = write(fd, buf, cnt);
                    if (s == -1)
                    {
                        perror("Write error");
                        exit(1);
                    }
                }

                if (flag)
                {
                    printf("Closed connection on descriptor %d\n", events[i].data.fd);
                    close(events[i].data.fd);
                }
                close(fd);
            }
        }
    }

    delete[] events;
    close(sock_fd);
    cout << "\nBye!\n" << endl;
    return 0;
}
